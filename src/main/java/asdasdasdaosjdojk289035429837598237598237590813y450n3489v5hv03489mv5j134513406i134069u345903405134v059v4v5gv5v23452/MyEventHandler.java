package asdasdasdaosjdojk289035429837598237598237590813y450n3489v5hv03489mv5j134513406i134069u345903405134v059v4v5gv5v23452;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.equo.comm.api.ICommService;
import com.equo.comm.api.actions.IActionHandler;
import com.equo.comm.api.annotations.EventName;

@Component
public class MyEventHandler implements IActionHandler {

	@Reference
	private ICommService commService;

	@EventName("MyEventHandler")
	public String myFirstEvent(String payload) {
		System.out.println("First event: " + payload);
		commService.send("MyJavaEvent", "This is your first message received from Java");
		return payload;
	}
}
